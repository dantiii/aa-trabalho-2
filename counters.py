import random
import sys
import math
import datetime

# ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
# Trabalho 2 - Algoritmos Avançados
# Prof.: Joaquim Madeira
# Aluno: Dante Marinho
# ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

test_file_path = 'output/test_file_'
output_length = 1000000  # number of characters in output file (test file)
num_times = 10  # number of trials used for both functions: prob_counter_one_half() and prob_counter_logaritmic()


# Returns the person name in name.txt file
# -------------------------------------------------------------------------------
def get_name():
    file_name = open('name.txt', 'r')
    name = file_name.readline()
    file_name.close()
    return name


# This function generates one output file with 100, 1000, 10000... chars
# -------------------------------------------------------------------------------
def generate_test_files():
    name = get_name()

    test_file = open(test_file_path + str(output_length) + '.txt', 'w')
    for i in range(output_length):
        choice = random.choice(name)
        test_file.write(choice)
    test_file.close()
generate_test_files() # geretaing test file


# Returns unic letters of the name
# -------------------------------------------------------------------------------
def get_unic_letters():
    name = get_name()
    unic_letters = list(set(name))
    unic_letters.sort()
    return unic_letters


# Returns the content (random letters) in one test file
# -------------------------------------------------------------------------------
def get_output(output_number):
    output_file = open(test_file_path + str(output_number) + '.txt', 'r')
    output = output_file.readline()
    return output

# Generates exact counting for each test file inside "output" folder
# -------------------------------------------------------------------------------    
# ("unic_letters" and "output" are needed vars to following functions)
unic_letters = get_unic_letters()  # example: ['a', 'd', 'e', 'f', 'h', 'i', 'm', 'n', 'o', 'r', 't']
output = get_output(output_length)  # example: dhtrnrirfhirrniaireadnntieoeteiaaeaarenanmehtmfnieririimdreteirnninfhrtontorndoorridrerdatetrfrmania

letter_statistics = {}

def exact_counter():

    for letter in unic_letters:
        letter_statistics[letter] = output.count(letter)

    print('\n\n~ ~ ~ Exact Counter #{} ~ ~ ~\n'.format(output_length))
    print('Letter | #\n------------')
    for key, value in letter_statistics.items():
        print('{:7}| {:}'.format(key, value))
    # print('Contador exato:', letter_statistics)
exact_counter()  # couting each char from input name


# Function :: Probabilistic counter 1/2
# -------------------------------------------------------------------------------
accumulated_letter_counter = {}
min_one_half = {}
max_one_half = {}
num_time_zero = True  # indicates that index cicle is zero

# Init temporary min and max vars with zeros
for letter in unic_letters:
    min_one_half[letter] = 0
    max_one_half[letter] = 0

def prob_counter_one_half():
    
    letter_prob_counter = {}

    for letter in unic_letters:
        for letter_output in output:
            if letter == letter_output:

                # ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                choice_prob = random.choice([0, 1])  # increment 1/2 of cases
                # ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

                if choice_prob == 1:

                    # fill letter counter
                    if letter in letter_prob_counter:
                        letter_prob_counter[letter] += 1
                    else:
                        letter_prob_counter[letter] = 1

                    # fill accumulator
                    if letter in accumulated_letter_counter:
                        accumulated_letter_counter[letter] += 1
                    else:
                        accumulated_letter_counter[letter] = 1

    for key, value in letter_prob_counter.items():

        # In the first loop, match min and max with values of the letters,
        # after the first loop will verify and compare the min and max
        if num_time_zero:
            min_one_half[key] = value
            max_one_half[key] = value
        else:
            if letter_prob_counter[key] < min_one_half[key]:
                min_one_half[key] = letter_prob_counter[key]
                # print('encontrou min')

            if letter_prob_counter[key] > max_one_half[key]:
                max_one_half[key] = letter_prob_counter[key]
                # print('encontrou max')

# Auxiliar function to run n times the prob_counter_one_half() function
# -------------------------------------------------------------------------------
def run_n_times_prob_counter_one_half(num_times):
    global num_time_zero

    # call the main function n times
    for i in range(num_times):
        if i > 0:
            num_time_zero = False
        prob_counter_one_half()

time_start = datetime.datetime.now()
run_n_times_prob_counter_one_half(num_times)  # var num_times initialized on the top
time_end = datetime.datetime.now()
# print('\nDic acumulado (1/2):', accumulated_letter_counter)

# Auxiliar function to calculate and return relative errors
def calc_relative_error(key, value):
    exact = letter_statistics[key]
    error_absolute = exact - value
    error_relative = error_absolute / exact
    return error_relative


# Calc average and show min, avg and max values
letters_average = accumulated_letter_counter
print('\n~ ~ ~ Table for Probabilistic Counter 1/2 ~ ~ ~\n({} repetitions and output file: {} chars)'.format(num_times, output_length))
print('\n          -----COUNTING----     ---RELATIVE ERRORS---')
print('Letter    min    avg    max  |  min      avg      max')
print('-----------------------------------------------------')
for key, value in sorted(accumulated_letter_counter.items()):
    letters_average[key] = letters_average[key] / num_times

    # Calculating relative errors
    min_error_relative = calc_relative_error(key, min_one_half[key])
    max_error_relative = calc_relative_error(key, max_one_half[key])
    avg_error_relative = calc_relative_error(key, letters_average[key])

    print('{:}{:10}{:9}{:6}{:11}%{:8}%{:7}%'.format(key, min_one_half[key], round(letters_average[key], 2), max_one_half[key],
                                                    round(min_error_relative * 100, 2), round(avg_error_relative * 100, 2), round(max_error_relative * 100, 2)))    

print('\nTotal execution time (prob counter 1/2):', (time_end - time_start))

# -------------------------------------------------------------------------------
# Function :: Probabilistic logaritmic counter 1/sqrt(2**c)
# -------------------------------------------------------------------------------
accumulated_letter_counter = {}  # reinitializing this vars (was used in prob_counter_one_half() function)
min_logaritmic = {}
max_logaritmic = {}
num_time_zero_log = True  # indicates that index cicle is zero

# Init temporary min and max vars with zeros
for letter in unic_letters:
    min_logaritmic[letter] = 0
    max_logaritmic[letter] = 0

def prob_counter_logaritmic():

    # output = get_output(output_length)  # example: dhtrnrirfhirrniaireadnntieoeteiaaeaarenanmehtmfnieririimdreteirnninfhrtontorndoorridrerdatetrfrmania
    letter_prob_counter = {}

    for i, letter in enumerate(unic_letters):

        # ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        base = 1 / math.sqrt(2 ** i)  # increases logarithmically
        # ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

        for letter_output in output:
            if letter == letter_output:
                rand_float = random.uniform(0, 1)
                if rand_float < base:

                    # fill letter counter
                    if letter in letter_prob_counter:
                        letter_prob_counter[letter] += 1
                    else:
                        letter_prob_counter[letter] = 1

                    # fill accumulator
                    if letter in accumulated_letter_counter:
                        accumulated_letter_counter[letter] += 1
                    else:
                        accumulated_letter_counter[letter] = 1

    for key, value in letter_prob_counter.items():

        # In the first loop, match min and max with values of the letters,
        # after the first loop will verify and compare the min and max
        if num_time_zero_log:
            min_logaritmic[key] = value
            max_logaritmic[key] = value
        else:
            if letter_prob_counter[key] < min_logaritmic[key]:
                min_logaritmic[key] = letter_prob_counter[key]
                # print('encontrou min')

            if letter_prob_counter[key] > max_logaritmic[key]:
                max_logaritmic[key] = letter_prob_counter[key]
                # print('encontrou max')


# Auxiliar function to run n times the prob_counter_one_half() function
# -------------------------------------------------------------------------------
def run_n_times_prob_counter_logaritmic(num_times):
    global num_time_zero_log

    # call the main function n times
    for i in range(num_times):
        if i > 0:
            num_time_zero_log = False
        prob_counter_logaritmic()

# num_times = 1000
time_start = datetime.datetime.now()
run_n_times_prob_counter_logaritmic(num_times)  # var num_times initialized on the top
time_end = datetime.datetime.now()

# Calc average and show min, avg and max values
letters_average = accumulated_letter_counter
print('\n~ ~ ~ Table for Probabilistic Counter (logaritmic) ~ ~ ~\n({} repetitions and output file: {} chars)'.format(num_times, output_length))
print('\n          -----COUNTING----     ---RELATIVE ERRORS---')
print('Letter    min    avg    max  |  min      avg      max')
print('-----------------------------------------------------')
for key, value in sorted(accumulated_letter_counter.items()):
    letters_average[key] = letters_average[key] / num_times

    # Calculating relative errors
    min_error_relative = calc_relative_error(key, min_logaritmic[key])
    max_error_relative = calc_relative_error(key, max_logaritmic[key])
    avg_error_relative = calc_relative_error(key, letters_average[key])

    print('{:}{:10}{:9}{:6}{:11}%{:8}%{:7}%'.format(key, min_logaritmic[key], round(letters_average[key], 2), max_logaritmic[key],
                                                    round(min_error_relative * 100, 2), round(avg_error_relative * 100, 2), round(max_error_relative * 100, 2)))

print('\nTotal execution time (prob logaritmic):', (time_end - time_start))


# ~ ~ ~ Link References ~ ~ ~
# https://www.w3schools.com/python/ref_list_sort.asp
# https://realpython.com/iterate-through-dictionary-python/
# https://industriahoje.com.br/como-calcular-erro-relativo


'''
Exemplo de resultado esperado para uma chamada ao script counters.py ::

~ ~ ~ Exact Counter #100 ~ ~ ~

Letter | #
------------
a      | 12
d      | 1
e      | 33
f      | 1
h      | 8
i      | 14
m      | 2
n      | 3
o      | 3
r      | 20
t      | 3

~ ~ ~ Table for Probabilistic Counter 1/2 ~ ~ ~
(10000 repetitions and output file: 100 chars)

          -----COUNTING----     ---RELATIVE ERRORS---
Letter    min    avg    max  |  min      avg      max
-----------------------------------------------------
a         1     6.01    12      91.67%   49.89%    0.0%
d         0      0.5     1      100.0%    49.7%    0.0%
e         5    16.59    28      84.85%   49.74%  15.15%
f         1      0.5     1        0.0%   50.01%    0.0%
h         1     3.97     8       87.5%   50.39%    0.0%
i         1     7.01    14      92.86%   49.96%    0.0%
m         1      1.0     2       50.0%   50.13%    0.0%
n         1     1.51     3      66.67%   49.81%    0.0%
o         1      1.5     3      66.67%   49.97%    0.0%
r         2     10.0    17       90.0%   50.01%   15.0%
t         1      1.5     3      66.67%   50.03%    0.0%

Total execution time (prob counter 1/2): 0:00:02.173186

~ ~ ~ Table for Probabilistic Counter (logaritmic) ~ ~ ~
(10000 repetitions and output file: 100 chars)

          -----COUNTING----     ---RELATIVE ERRORS---
Letter    min    avg    max  |  min      avg      max
-----------------------------------------------------
a        12     12.0    12        0.0%     0.0%    0.0%
d         1      0.7     1        0.0%   29.69%    0.0%
e         6    16.49    28      81.82%   50.03%  15.15%
f         1     0.36     1        0.0%   64.38%    0.0%
h         1     1.99     7       87.5%   75.14%   12.5%
i         1     2.48     9      92.86%    82.3%  35.71%
m         1     0.24     2       50.0%    87.8%    0.0%
n         0     0.26     3      100.0%   91.21%    0.0%
o         1     0.19     3      66.67%   93.81%    0.0%
r         1     0.89     6       95.0%   95.56%   70.0%
t         0     0.09     2      100.0%   97.09%  33.33%

Total execution time (prob counter logaritmic): 0:00:01.188898
'''