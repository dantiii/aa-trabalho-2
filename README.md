# Algoritmos Avançados | Trabalho 2 - Hipótese B – Contagem Aproximada de Ocorrências – Cadeias de Caracteres Aleatórios.

Pretende-se que sejam criados métodos para contar o número de ocorrência de caracteres em cadeias de caracteres aleatórios.

Docente: Joaquim Madeira

Aluno: Dante Marinho (83672)

## Como Correr o Programa

Digitar o seguinte comando na linha de comandos:

python counters.py

## Observações

No topo do script poderá alterar as seguintes variáveis:

- test_file_path (nome do ficheiro de output contendo os caracteres aleatórios)
- output_length (número de caracteres que deverá ter o ficheiro de output (test file))
- num_times (número de vezes que as funções probabilísticas deverá correr)