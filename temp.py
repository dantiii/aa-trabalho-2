import random
import math

name = 'danteferreira'
unic_letters = list(set(name))
unic_letters.sort()

print('Name:', name)
print('Tuplo:', unic_letters)

c = 'dannnte'
count = c.count('a')
print('count', count)

print('CHOICE')
n = [0, 1]
choice = random.choice([0, 1])
print('Choice:', choice)


dic1 = {'a': 1, 'd': 3}
dic2 = {'a': 2, 'd': 4}

# dic3 = dic1 + dic2
# print('Soma dic1 + dic2', dic3)

d1 = {'a': 11, 'd': 3, 'e': 12, 'f': 2, 'h': 4, 'i': 17, 'm': 4, 'n': 12, 'o': 2, 'r': 21, 't': 9}
d2 = {'a': 5, 'd': 1, 'e': 4, 'f': 1, 'h': 2, 'i': 7, 'm': 2, 'n': 6, 'o': 1, 'r': 8, 't': 3}
d3 = {'a': 6, 'd': 2, 'e': 8, 'f': 1, 'h': 2, 'i': 10, 'm': 2, 'n': 6, 'o': 1, 'r': 13, 't': 6}

d0 = {'a': 5495, 'd': 4090, 'e': 6064,  'f': 4029,  'h': 2419,  'i': 4496,  'm': 4032,  'n': 4506,          'r': 11370, 't': 1484, 'o': 2013}
d4 = {'a': 1, 'd': 1,       'e': 1,     'f': 1,     'h': 1,     'i': 1,     'm': 1,     'n': 1,     'o': 0, 'r': 3,     't': 1}
d5 = {'a': 10, 'd': 8,      'e': 11,    'f': 8,     'h': 5,     'i': 9,     'm': 8,     'n': 9,     'o': 4, 'r': 18,    't': 3}

d = zip(d1, d2, d3)

print('d', d)

for k, v in sorted(d0.items()):
    print(k, v)

# Experimentacao para mudar o ciclo FOR para ciclo WHILE
print('\nCICLOS PROBABILISTICOS\n')

lista = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']

num_vezes = 0
loops = len(lista)
i = 0
while i < len(lista):
    num_vezes += 1
    if random.choice([0, 1]) == 0:
        print('fica', i, lista[i])
    else:
        print('anda p próx. letra', i, lista[i])
        i += 1
print('Num vezes:', num_vezes)


# Prob logaritmica
print('\nCICLO PROBABILISTICO LOG\n')

lista = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'c', 'd', 'e','a', 'f', 'a', 'a', 'g', 'h', 'a', 'i', 'a', 'j', 'a', 'a', 'a']

num_vezes = 0
loops = len(lista)
for i in range(len(lista)):
    base = 1 / math.sqrt(2 ** i)

    print('\nProbabilidade:', base)
    num_vezes += 1

    rand = random.uniform(0, 1)
    print('Valor do rand:', rand)

    if rand < base:
        print('conta', i, lista[i])
    else:
        print('não conta', i, lista[i])
print('Num vezes:', num_vezes)


for i, letra in enumerate(lista):
    print('Letrinha', lista[i], 'indice:', i )
